package geekbrains.zero.myapplication.view.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class CounterModel {
    private List<Integer> counters;
    final String LOG_TAG = "myLogs";

    public CounterModel() {
        counters = new ArrayList<>();
        counters.add(0);
        counters.add(0);
        counters.add(0);
    }

    public Observable<Integer> calculate(int index) {

        return Observable.fromCallable(() -> {
            int value = counters.get(index) + 1;
            counters.set(index, value);
            return value;
        });
    }

    public Observable<Boolean> converted(Uri uri, Context context) {
        return Observable.fromCallable(() -> {
            if (!Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                Log.d(LOG_TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            }
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            File convertedImage = new File(Environment.getExternalStorageDirectory() + "/convertedimg.png");
            FileOutputStream outStream = new FileOutputStream(convertedImage);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
            return true;
        });
    }
}
