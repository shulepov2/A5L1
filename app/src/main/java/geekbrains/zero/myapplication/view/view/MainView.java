package geekbrains.zero.myapplication.view.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(value = AddToEndSingleStrategy.class)
public interface MainView extends MvpView {

    void setThreeValue(int value);

    void setTwoValue(int value);

    void setOneValue(int value);

    void statusConvert(Boolean value);
}
