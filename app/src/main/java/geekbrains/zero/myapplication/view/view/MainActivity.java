package geekbrains.zero.myapplication.view.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.jakewharton.rxbinding.widget.RxTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import geekbrains.zero.myapplication.R;
import geekbrains.zero.myapplication.view.model.CounterModel;
import geekbrains.zero.myapplication.view.presenter.MainPresenter;
import rx.Subscription;


public class MainActivity extends MvpAppCompatActivity implements MainView {

    static final int GALLERY_REQUEST = 1;
    boolean success = false;

    @BindView(R.id.btn_one)
    Button buttonOne;
    @BindView(R.id.btn_two)
    Button buttonTwo;
    @BindView(R.id.btn_three)
    Button buttonThree;
    @BindView(R.id.edit_text)
    EditText editText;
    @BindView(R.id.image_convert)
    Button convertButton;
    @BindView(R.id.text_view)
    TextView textView;
    @BindView(R.id.edit_text2)
    EditText editText2;
    @BindView(R.id.text_view2)
    TextView textView2;

    @InjectPresenter
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        editTextListener();
    }

    private void editTextListener() {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textView.setText(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        Subscription editTextSub = RxTextView.textChanges(editText2).subscribe(value -> textView2.setText(value));

        convertButton.setOnClickListener(v -> {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, GALLERY_REQUEST);

        });
    }

    @ProvidePresenter
    public MainPresenter provideMainPresenter() {
        return new MainPresenter(new CounterModel());
    }

    @OnClick({R.id.btn_one, R.id.btn_two, R.id.btn_three})
    public void onButtonClick(Button button) {
        switch (button.getId()) {
            case R.id.btn_one:
                presenter.buttonOneClick(0);
                break;
            case R.id.btn_two:
                presenter.buttonTwoClick(1);
                break;
            case R.id.btn_three:
                presenter.buttonThreeClick(2);
                break;
        }
    }


    @Override
    public void setThreeValue(int value) {
        buttonThree.setText(String.valueOf(value));
    }

    @Override
    public void setTwoValue(int value) {
        buttonTwo.setText(String.valueOf(value));
    }

    @Override
    public void setOneValue(int value) {
        buttonOne.setText(String.valueOf(value));
    }

    @Override
    public void statusConvert(Boolean success) {
        if (success) {
            Toast.makeText(this, "Converting is successful.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Converting is unsucessful.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GALLERY_REQUEST:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    presenter.convertImage(selectedImage, this);
                }
        }
    }
}