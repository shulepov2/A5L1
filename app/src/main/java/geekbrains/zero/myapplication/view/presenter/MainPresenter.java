package geekbrains.zero.myapplication.view.presenter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import geekbrains.zero.myapplication.view.model.CounterModel;
import geekbrains.zero.myapplication.view.view.MainView;


@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {
    private CounterModel model;

    public MainPresenter(CounterModel model) {
        this.model = model;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    @SuppressLint("CheckResult")
    public void convertImage(Uri uri, Context context) {
        model.converted(uri, context).subscribe(Boolean -> getViewState().statusConvert(true));
    }

    @SuppressLint("CheckResult")
    public void buttonOneClick(int index) {
        model.calculate(index).subscribe(integer -> getViewState().setOneValue(integer));
    }

    @SuppressLint("CheckResult")
    public void buttonTwoClick(int index) {
        model.calculate(index).subscribe(integer -> getViewState().setTwoValue(integer));
    }

    @SuppressLint("CheckResult")
    public void buttonThreeClick(int index) {
        model.calculate(index).subscribe(integer -> getViewState().setThreeValue(integer));
    }
}
